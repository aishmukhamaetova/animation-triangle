import React from "react";
import { useSpring, animated } from "react-spring";
import "./styles.css";

const defaults = {
  x: 10,
  y: 10,
  width: 80,
  height: 80,
  duration: 500,
};

function KeyTop({ onFinish }) {
  const { x, y, width, height, duration } = defaults;

  const sText = useSpring({
    from: { opacity: 0 },
    to: { opacity: 1 },
    config: { duration },
  });

  const clip = useSpring({
    from: { value: 24 },
    to: { value: 100 },
    config: { duration },
    onRest: onFinish,
  });

  return (
    <>
      <clipPath id="top">
        <animated.rect x="0" y="0" width={100} height={clip.value} />
      </clipPath>

      <image
        x={x}
        y={y}
        width={width}
        height={height}
        xlinkHref="./dot_top.svg"
      />
      <image
        x={x}
        y={y}
        width={width}
        height={height}
        xlinkHref="./line_right.svg"
        clip-path="url(#top)"
      />

      <animated.text
        x={42}
        y={15}
        className="text"
        style={{ opacity: sText.opacity }}
      >
        TIME
      </animated.text>
    </>
  );
}

function KeyRight({ onFinish }) {
  const { x, y, width, height, duration } = defaults;

  const sText = useSpring({
    from: { opacity: 0 },
    to: { opacity: 1 },
    config: { duration },
  });

  const clip = useSpring({
    from: { value: 85 },
    to: { value: 0 },
    config: { duration },
    onRest: onFinish,
  });

  return (
    <>
      <clipPath id="right">
        <animated.rect x={clip.value} y="0" width={100} height={100} />
      </clipPath>

      <image
        x={x}
        y={y}
        width={width}
        height={height}
        xlinkHref="./dot_right.svg"
      />
      <image
        x={x}
        y={y}
        width={width}
        height={height}
        xlinkHref="./line_bottom.svg"
        clip-path="url(#right)"
      />

      <animated.text
        x={73}
        y={87}
        className="text"
        style={{ opacity: sText.opacity }}
      >
        QUALITY
      </animated.text>
    </>
  );
}

function KeyLeft({ onFinish }) {
  const { x, y, width, height, duration } = defaults;

  const sText = useSpring({
    from: { opacity: 0 },
    to: { opacity: 1 },
    config: { duration },
  });

  const clip = useSpring({
    from: { value: 85 },
    to: { value: 0 },
    config: { duration },
    onRest: onFinish,
  });

  return (
    <>
      <clipPath id="left">
        <animated.rect x={0} y={clip.value} width={100} height={100} />
      </clipPath>

      <image
        x={x}
        y={y}
        width={width}
        height={height}
        xlinkHref="./dot_left.svg"
      />
      <image
        x={x}
        y={y}
        width={width}
        height={height}
        xlinkHref="./line_left.svg"
        clip-path="url(#left)"
      />

      <animated.text
        x={6}
        y={87}
        className="text"
        style={{ opacity: sText.opacity }}
      >
        COST
      </animated.text>
    </>
  );
}

export default function App() {
  const [value, setValue] = React.useState(-1);

  React.useEffect(() => {
    if (value === -1) {
      setTimeout(() => setValue(0), defaults.duration);
    }
  }, [value]);

  return (
    <svg
      viewBox="0 0 100 100"
      xmlns="http://www.w3.org/2000/svg"
      style={{ maxWidth: 400, maxHeight: 400 }}
    >
      {value > -1 ? (
        <KeyTop onFinish={() => setValue(() => (value === 0 ? 1 : value))} />
      ) : null}
      {value > 0 ? (
        <KeyRight onFinish={() => setValue(() => (value === 1 ? 2 : value))} />
      ) : null}
      {value > 1 ? <KeyLeft /> : null}
      <text x={38} y={59} className="text" fill="blue">
        SAFETY
      </text>
    </svg>
  );
}
